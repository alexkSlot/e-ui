import actionTypes from "../../actionTypes";

const sectionButtonButtonClicked = (sectionId) => {
    return {
        type: actionTypes.SECTION_BUTTON_CLICKED,
        selectedSection: sectionId
    };
};

export const sectionButtonClickHandler = (sectionId) => {

    return dispatch => {
        dispatch(sectionButtonButtonClicked(sectionId));
    };
};

const gameSettingChanged = (settingId) => {
    return {
        type: actionTypes.GAME_SETTING_CHANGED,
        settingId: settingId
    };
};

export const gameSettingClickHandler = (settingId) => {

    return dispatch => {
        dispatch(gameSettingChanged(settingId));
    };
};