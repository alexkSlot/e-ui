import actionTypes from "../../actionTypes";
import {API} from "../../../../api/api";
import {store} from "../../../";
import {OuterApiEvents as o} from "../../../../api/outerApiEvents";


API.on(o.updateGameSettings, (value) => {
    store.dispatch({
        type: actionTypes.UPDATE_GAME_SETTINGS,
        value
    });
});
