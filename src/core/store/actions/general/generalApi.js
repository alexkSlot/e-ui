import actionTypes from "../../actionTypes";
import {API} from "../../../../api/api";
import {store} from "../../../";
import {OuterApiEvents as o} from "../../../../api/outerApiEvents";


API.on(o.general.setCurrency, (value) => {
    store.dispatch({
        type: actionTypes.SET_CURRENCY,
        value
    });
});

API.on(o.general.setBalance, (value) => {
    store.dispatch({
        type: actionTypes.SET_BALANCE,
        value
    });
});

API.on(o.general.setWin, (value) => {
    store.dispatch({
        type: actionTypes.SET_WIN,
        value
    });
});