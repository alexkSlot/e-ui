import actionTypes from "../../actionTypes";
import {API}from "../../../../api/api";

const spinButtonClicked = () => {
    return {
        type: actionTypes.SPIN_BUTTON_CLICKED
    };
};

export const spinButtonClickHandler = () => {

    return dispatch => {
        dispatch(spinButtonClicked());
    };
};

export const toggleSoundButton  = () =>{
     return dispatch => {
        dispatch({
            type: actionTypes.SOUND_BUTTON_CLICKED
        });
    };
};

export const toggleMenuButton  = () =>{
    return dispatch => {
        dispatch({
            type: actionTypes.MENU_BUTTON_CLICKED
        });
    };
};