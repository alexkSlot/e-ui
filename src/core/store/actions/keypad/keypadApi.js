import actionTypes from "../../actionTypes";
import {API} from "../../../../api/api";
import {store} from "../../../";
import {OuterApiEvents as o} from "../../../../api/outerApiEvents";


API.on(o.keypad.enable, () => {
    store.dispatch({
        type: actionTypes.KEYPAD_ENABLE
    });
});

API.on(o.keypad.disable, () => {
    store.dispatch({
        type: actionTypes.KEYPAD_DISABLE
    });
});

API.on(o.spinButton.enable, () => {
    store.dispatch({
        type: actionTypes.ENABLE_SPIN_BUTTON
    });
});

API.on(o.spinButton.disable, () => {
    store.dispatch({
        type: actionTypes.DISABLE_SPIN_BUTTON
    });
});

API.on(o.autoPlayButton.disable, () => {
    store.dispatch({
        type: actionTypes.AUTO_PLAY_BUTTON_DISABLE
    });
});

API.on(o.autoPlayButton.enable, () => {
    store.dispatch({
        type: actionTypes.AUTO_PLAY_BUTTON_ENABLE
    });
});

API.on(o.betButton.enable, () => {
    store.dispatch({
        type: actionTypes.BET_BUTTON_ENABLE
    });
});

API.on(o.betButton.disable, () => {
    store.dispatch({
        type: actionTypes.BET_BUTTON_DISABLE
    });
});

API.on(o.menuButton.enable, () => {
    store.dispatch({
        type: actionTypes.MENU_BUTTON_ENABLE
    });
});

API.on(o.menuButton.disable, () => {
    store.dispatch({
        type: actionTypes.MENU_BUTTON_DISABLE
    });
});

API.on(o.soundButton.enable, () => {
    store.dispatch({
        type: actionTypes.SOUND_BUTTON_ENABLE
    });
});

API.on(o.soundButton.disable, () => {
    store.dispatch({
        type: actionTypes.SOUND_BUTTON_DISABLE
    });
});