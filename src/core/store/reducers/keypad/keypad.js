import actionTypes from "../../actionTypes";

const initialState = {
    spinButtonEnabled: true,
    autoPlayButtonEnabled: true,
    keypadEnabled: true,
    betButtonEnabled: true,
    menuButtonEnabled: true,
    soundButtonEnabled: true,
    soundEnabled: true
};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.KEYPAD_ENABLE:
            return {
                ...state,
                keypadEnabled: true
            };

        case actionTypes.KEYPAD_DISABLE:
            return {
                ...state,
                keypadEnabled: false
            };

        // case actionTypes.SPIN_BUTTON_CLICKED:
        //     return {
        //         ...state,
        //         spinButtonEnabled: false
        //     };

        case actionTypes.ENABLE_SPIN_BUTTON:
            return {
                ...state,
                spinButtonEnabled: true
            };

        case actionTypes.DISABLE_SPIN_BUTTON:
            return {
                ...state,
                spinButtonEnabled: false
            };

        case actionTypes.AUTO_PLAY_BUTTON_ENABLE:
            return {
                ...state,
                autoPlayButtonEnabled: true
            };

        case actionTypes.AUTO_PLAY_BUTTON_DISABLE:
            return {
                ...state,
                autoPlayButtonEnabled: false
            };

        case actionTypes.BET_BUTTON_ENABLE:
            return {
                ...state,
                betButtonEnabled: true
            };

        case actionTypes.BET_BUTTON_DISABLE:
            return {
                ...state,
                betButtonEnabled: false
            };

        case actionTypes.MENU_BUTTON_ENABLE:
            return {
                ...state,
                menuButtonEnabled: true
            };

        case actionTypes.MENU_BUTTON_DISABLE:
            return {
                ...state,
                menuButtonEnabled: false
            };

        case actionTypes.SOUND_BUTTON_ENABLE:
            return {
                ...state,
                soundButtonEnabled: true
            };

        case actionTypes.SOUND_BUTTON_DISABLE:
            return {
                ...state,
                soundButtonEnabled: false
            };

            case actionTypes.SOUND_BUTTON_CLICKED:
            return {
                ...state,
                soundEnabled: !state.soundEnabled
            };

        default:
            return state;
    }

};

export default reducer;