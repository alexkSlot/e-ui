import {combineReducers} from "redux";

import generalReducer from "./general/general";
import keypadReducer from "./keypad/keypad";
import menuReducer from "./menu/menu";

export const rootReducer = combineReducers({
    general: generalReducer,
    keypad: keypadReducer,
    menu: menuReducer
});