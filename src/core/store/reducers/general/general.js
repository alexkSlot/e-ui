import actionTypes from "../../actionTypes";

const initialState = {
    currency: {
        iso: 'USD',
        symbol: '$',
        decimals: 2,
        decimalDivider: '.',
        thousandsDivider: ','
    },

    balance: 0,
    win: 0
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SET_CURRENCY:
            return {
                ...state,
                currency: action.value
            };

        case actionTypes.SET_BALANCE:
            return {
                ...state,
                balance: action.value
            };

        case actionTypes.SET_WIN:
            return {
                ...state,
                win: action.value
            };

        default:
            return state;
    }

};

export default reducer;