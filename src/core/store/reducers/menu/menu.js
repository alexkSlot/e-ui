import actionTypes from "../../actionTypes";
import {API} from "../../../../api/api";
import {OuterApiEvents} from "../../../../api/outerApiEvents";

const initialState = {
    menuActive: false,
    activeSection: "info", // settings, info, gameRules, gameHistory
    gameSettings: [
        {
            id: "featureSplashEnabled",
            checked: false,
            type: "checkbox",
            label: "Show feature splash",
            icon: "featureSplash"
        },
        {
            id: "quickSpinEnabled",
            checked: true,
            type: "checkbox",
            label: "Quick spin enabled",
            icon: "quickSpin"
        }
    ]
};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.MENU_BUTTON_CLICKED:
            return {
                ...state,
                menuActive: !state.menuActive
            };

        case actionTypes.SPIN_BUTTON_CLICKED:
            return {
                ...state,
                menuActive: false
            };

        case actionTypes.SECTION_BUTTON_CLICKED:
            return {
                ...state,
                activeSection: action.selectedSection
            };

        case actionTypes.UPDATE_GAME_SETTINGS:

            console.log( action.value);
            return {
                ...state,
                gameSettings: action.value
            };

        case actionTypes.GAME_SETTING_CHANGED:
            const settings = [].concat(state.gameSettings);
            const settingIndex = settings.findIndex((el) => el.id === action.settingId);
            const setting = { ...settings[settingIndex] };

            setting.checked = !setting.checked;
            settings[settingIndex] = setting;

            API.emit(OuterApiEvents.gameSettingChanged, { settingId: action.settingId, value: setting.checked });

            return {
                ...state,
                gameSettings: settings
            };

        default:
            return state;
    }

};

export default reducer;