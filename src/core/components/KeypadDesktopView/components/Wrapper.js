import styled from "styled-components";

export const Wrapper = styled.div`
    height: calc(0.7 * 8vw);
    background: #424242;
    position: relative;
    display: flex;
    justify-content: space-around;
    align-items: center;
    z-index: 5;
   
`;