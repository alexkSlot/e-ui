import styled from "styled-components";

export const SpinSectionWrapper = styled.div`
         position: relative;
         width: 30%;
         height: 100%;
         background-color: #6f6d6d;
         border-radius: 35px;
         display: flex;
         align-items: center;
         justify-content: space-around;
   
`;