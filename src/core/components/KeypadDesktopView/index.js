import React from "react";

import {Wrapper} from "./components/Wrapper";
import {SpinSectionWrapper} from "./components/SpinSectionWrapper";

import {APINames} from "../../../api/apiNames";
import {addCurrencyPrefix} from "../../../utils/utils";
import {currencyFormatter} from "../../../utils/currencyFormatter/currencyFormatter";

import {MenuButton} from "../ui/MenuButton/index";
import {SoundButton} from "../ui/SoundButton/index";
import {SpinButton} from "../ui/SpinButton";
import {AutoplayButton} from "../ui/AutoplayButton";
import {BetButton} from "../ui/BetButton/index";
import {KeypadInfoSection} from "../ui/KeypadInfoSection/KeypadInfoSection";

const keypadDesktopView = ({
                               onSpinButtonClicked,
                               toggleSoundButton,
                               toggleMenuButton,

                               keypadDisabled,
                               autoPlayButtonDisabled,
                               spinButtonDisabled,
                               betButtonDisabled,
                               menuButtonDisabled,
                               soundButtonDisabled,
                               soundEnabled,

                               currency,
                               balance,
                               win
                           }) => {

    return (
        <Wrapper>
            <MenuButton apiName={APINames.MENU_BUTTON}
                        disabled={menuButtonDisabled || keypadDisabled}
                        onClick={toggleMenuButton}/>

            <KeypadInfoSection label={addCurrencyPrefix("Cash", currency.iso)} textValue={currencyFormatter(balance, currency)}/>

            <SpinSectionWrapper>
                <AutoplayButton apiName={APINames.AUTO_PLAY_BUTTON}
                                disabled={autoPlayButtonDisabled || keypadDisabled}/>

                <SpinButton apiName={APINames.SPIN_BUTTON}
                            disabled={spinButtonDisabled || keypadDisabled}
                            onClick={onSpinButtonClicked}/>

                <BetButton apiName={APINames.BET_BUTTON}
                           disabled={betButtonDisabled || keypadDisabled}/>
            </SpinSectionWrapper>

            <KeypadInfoSection label={addCurrencyPrefix("Win", currency.iso)} textValue={currencyFormatter(win, currency)}/>
            <SoundButton apiName={APINames.SOUND_BUTTON}
                         disabled={soundButtonDisabled}
                         onClick={toggleSoundButton}
                         soundEnabled={soundEnabled}/>
        </Wrapper>

    );
};

export default keypadDesktopView;