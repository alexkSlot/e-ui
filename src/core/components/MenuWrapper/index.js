import styled from "styled-components";

export const MenuDesktopWrapper = styled.div`
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
    height: calc( 100% - (0.7 * 8vw));
    background: #464545;
    user-select: none;
`;