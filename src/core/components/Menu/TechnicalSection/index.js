import React from "react";
import {Wrapper} from "./components/Wrapper";

export const TechnicalSection = (props) => {
    return (
        <Wrapper>
            { props.children}
        </Wrapper>
    );
};
