import React, {Component} from "react";
import {MenuDesktopWrapper} from "../../MenuWrapper/index";
import {Wrapper} from "./components/Wrapper";

import {ButtonsSection} from "../ButtonsSection/index";
import {MenuContentSection} from "../MenuContentSection/index";
import {TechnicalSection} from "../TechnicalSection/index";

import {GameSettingItem} from "../../ui/GameSettingItem/GameSettingItem";

export class MenuDesktop extends Component  {

    render(){
        const renderElement = this.props.menuActive ?
            (<MenuDesktopWrapper>
                <Wrapper>
                    <ButtonsSection
                        activeSection={this.props.activeSection}
                        onSectionButtonClicked={this.props.onSectionButtonClicked}/>

                    <MenuContentSection>
                        MENU CONTENT SECTION
                        {this.props.gameSettings.map((togConf)=>{return (
                            <GameSettingItem
                                key={togConf.id}
                                onChange={()=>{this.props.onGameSettingChanged(togConf.id)}}
                                {...togConf}/>
                        )})}


                    </MenuContentSection>

                    <TechnicalSection>Technical field</TechnicalSection>
                </Wrapper>

            </MenuDesktopWrapper>)
            : null;

        return renderElement;
    }
}

