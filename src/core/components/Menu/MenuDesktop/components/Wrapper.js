import styled from "styled-components";

export const Wrapper = styled.div`
    display: flex;
    flex: 1 1 100%;
    flex-direction: row;
    width: 100%;
    height: 100%;
`;