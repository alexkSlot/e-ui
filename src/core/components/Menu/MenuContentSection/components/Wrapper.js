import styled from "styled-components";

export const Wrapper = styled.div`

    flex: 1 1 auto;
    position: relative;
    box-sizing: border-box;
`;