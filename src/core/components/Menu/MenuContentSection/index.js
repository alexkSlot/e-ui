import React from "react";
import {Wrapper} from "./components/Wrapper";

export const MenuContentSection = (props) => {
    return (
        <Wrapper>
            {props.children}
        </Wrapper>
    );
};
