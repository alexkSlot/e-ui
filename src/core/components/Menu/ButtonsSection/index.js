import React from "react";
import {Wrapper} from "./components/Wrapper";

import {SettingButton} from "../../ui/Menu/MenuButtons/SettingsButton";
import {InfoButton} from "../../ui/Menu/MenuButtons/InfoButton/index";

export const ButtonsSection = ({ activeSection, onSectionButtonClicked }) => {
    return (
        <Wrapper>
            <SettingButton
                active={activeSection === "settings"}
                onClick={() => {
                    onSectionButtonClicked("settings")
                }}/>
            <InfoButton
                active={activeSection === "info"}
                onClick={() => {
                    onSectionButtonClicked("info")
                }}/>
        </Wrapper>
    );
};
