import styled from "styled-components";

export const Wrapper = styled.div`
    box-sizing: border-box;
    display: inherit;
    flex: 0 0 9%;
    flex-direction: column;
    justify-content: flex-start;
    position: relative;
    overflow: hidden;
`;