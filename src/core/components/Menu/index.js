import React from "react";
import {MenuDesktop} from "./MenuDesktop";

export const Menu = (props) => {
    return <MenuDesktop {...props}/>
};
