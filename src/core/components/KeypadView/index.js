import React, {Component} from "react";

import {Wrapper} from "./components/Wrapper";
import KeypadDesktopView from "../KeypadDesktopView";

export default class KeypadView extends Component {

    render() {
        return (
            <Wrapper>
                <KeypadDesktopView {...this.props}/>
            </Wrapper>
        );
    }
}