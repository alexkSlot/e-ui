import React from "react";
import {Wrapper} from "./components/Wrapper";
import {AutoplayIcon} from "./components/Icon";

export const AutoplayButton = (props) => {
    return <Wrapper {...props}>
        <AutoplayIcon />
    </Wrapper>;
};
