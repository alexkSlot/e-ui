import React from "react";
import {Wrapper} from "./components/Wrapper";
import {BetButtonIcon} from "./components/Icon";

export const BetButton = (props) => {
    return <Wrapper {...props}>
        <BetButtonIcon />
    </Wrapper>;
};
