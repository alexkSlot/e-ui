import {DefaultIcon} from "./DefaultIcon";
import {FeatureSplash} from "./FeatureSplash";
import {QuickSpinIcon} from "./QuickSpinIcon";
import {IntroMovieIcon} from "./IntroMovieIcon";

const icons = {
    defaultIcon: DefaultIcon,
    featureSplash: FeatureSplash,
    quickSpin: QuickSpinIcon,
    introMovie: IntroMovieIcon
};

export const getIconById = iconId => icons[iconId] || icons.defaultIcon;