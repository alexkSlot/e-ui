import React from "react";
import {BaseSVG} from "../BaseSVG/index";

export const FeatureSplash = () => {
    return (<BaseSVG viewBox="0 0 60 60">
        <g>
            <path d="M18.195,55.5h23.609L30,36.613L18.195,55.5z M21.805,53.5L30,40.387L38.195,53.5H21.805z"/>
            <path d="M0,4.5v40h22c0.553,0,1-0.447,1-1s-0.447-1-1-1H2v-36h56v36H38c-0.553,0-1,0.447-1,1s0.447,1,1,1h22v-40H0z"/>
        </g>

    </BaseSVG>);
};
