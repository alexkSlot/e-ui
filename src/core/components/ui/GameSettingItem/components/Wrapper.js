import styled from "styled-components";

export const Wrapper = styled.div`
       display: flex;
       align-items: center;
       padding: 1.5em 0;
       box-sizing: content-box;
       color: white;
`;
