import styled from "styled-components";

export const IconWrapper = styled.div`
    flex: 0 0 4vw; 
    padding-right: 3.3vw;
    color: #bbb;
`;
