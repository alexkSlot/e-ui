import styled from "styled-components";

export const ToggleWrapper = styled.div`
    flex: 0 0 30%;
    display: inline-flex;
`;
