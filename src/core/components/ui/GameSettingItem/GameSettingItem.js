import React from "react";

import {Wrapper} from "./components/Wrapper";
import {IconWrapper} from "./components/IconWrapper";
import {LabelWrapper} from "./components/LabelWrapper";
import {ToggleWrapper} from "./components/ToggleWrapper";
import {SettingToggle} from "../SettingToggle/index";
import {getIconById} from "../IconLibrary/index";

export const GameSettingItem = (props) => {
    const Icon = getIconById(props.icon);

    return (
        <Wrapper>
            <IconWrapper onClick={props.onChange}><Icon/></IconWrapper>

            <LabelWrapper>
                <span onClick={props.onChange}>{props.label}</span>
            </LabelWrapper>

            <ToggleWrapper>
                <SettingToggle {...props}/>
            </ToggleWrapper>

        </Wrapper>

    );
};