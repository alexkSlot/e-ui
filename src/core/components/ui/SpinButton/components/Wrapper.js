import styled from "styled-components";
import {BaseButton} from "../../BaseButton/index";

export const Wrapper = styled(BaseButton)`
  width: 8vw;
  height: 8vw;
  border: 1px solid white
  border-radius: 50%;
  position: relative;
  background: #424242;
  color: white;
  bottom: 22%;
  cursor: pointer;

  &:hover {
    background: #585858;
  }

  &:active {
    background: #1d1d1d;
  }

@keyframes rotation {
    from {transform: rotate(0deg);}
    to {transform: rotate(360deg);}
}

  &:hover svg{
  	animation-name: rotation;
    animation-duration: 0.5s;
  }

  &[disabled] {
    background: #ccc;
    color: #afafaf;
    cursor: auto;
  }
`;
