import React from "react";
import {Wrapper} from "./components/Wrapper";
import {SpinIcon} from "./components/Icon";

export const SpinButton = (props) => {
    return <Wrapper {...props}>
        <SpinIcon />
    </Wrapper>;
};
