import styled from "styled-components";
import {BaseButton} from "../../BaseButton/index";

export const Wrapper = styled(BaseButton)`
  width: 5vw;
  height: 5vw;
  background: none;
  position: relative;
  color: white;
  cursor: pointer;
  border: none;
  box-sizing: border-box;
  min-width: 25px;
  min-height: 25px;
  
  &:hover {
   color: #ccc;
  }

  &:active {
    color: #b9b5b5;
  }

  &[disabled] {
   color: red;
    cursor: auto;
  }
`;
