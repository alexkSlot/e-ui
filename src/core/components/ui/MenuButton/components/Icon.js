import React from "react";
import {BaseSVG} from "../../BaseSVG/index";

export const MenuButtonIcon = (props) => {
    return (<BaseSVG viewBox="0 0 512 512" {...props}>
        <g>
            <path d="M32 70h448v70h-448zM32 210h448v70h-448zM32 352h448v70h-448z"></path>
        </g>

    </BaseSVG>);
};
