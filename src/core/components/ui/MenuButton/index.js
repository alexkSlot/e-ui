import React from "react";
import {Wrapper} from "./components/Wrapper";
import {MenuButtonIcon} from "./components/Icon";

export const MenuButton = (props) => {
    return <Wrapper {...props}>
        <MenuButtonIcon />
    </Wrapper>;
};
