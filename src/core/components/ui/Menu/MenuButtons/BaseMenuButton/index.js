import React from "react";
import {Wrapper} from "./components/Wrapper";

export const BaseMenuButton = (props) => {
    return <Wrapper {...props}>
        {props.children}
    </Wrapper>;
};
