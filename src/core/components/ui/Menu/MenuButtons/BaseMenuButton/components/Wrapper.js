import styled from "styled-components";
import {BaseButton} from "../../../../BaseButton/index";

export const Wrapper = styled(BaseButton)`
    box-sizing: border-box;
    display: inherit;
    flex: 1 0 25%;
    align-items: center;
    justify-content: center;
    position: relative;
    color: white;
    cursor: pointer;
    border: none;
    background-color: #464545;
    background-color: ${props => props.active ? '#545353' : '#464545'};
    border-left: 2px solid ${props => props.active ? '#929292' : '#464545'};
  
    &:hover {
    background-color: #545353;
    }
    
    &:active {
    background-color: #3a3939;
    color: #b9b5b5;
    }
    
    &[disabled] {
     color: red;
      cursor: auto;
  }
`;
