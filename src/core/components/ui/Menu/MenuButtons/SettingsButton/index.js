import React from "react";
import {BaseMenuButton} from "../BaseMenuButton/index";
import {SettingButtonIcon} from "./components/Icon";

export const SettingButton = (props) => {
    return (
        <BaseMenuButton {...props}>
            <SettingButtonIcon/>
        </BaseMenuButton>);
};
