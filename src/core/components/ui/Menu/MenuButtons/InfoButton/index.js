import React from "react";
import {BaseMenuButton} from "../BaseMenuButton/index";
import {InfoButtonIcon} from "./components/Icon";

export const InfoButton = (props) => {
    return (
        <BaseMenuButton {...props}>
            <InfoButtonIcon/>
        </BaseMenuButton>);
};
