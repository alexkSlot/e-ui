import React from "react";
import {Wrapper} from "./components/Wrapper";
import {SoundButtonActiveIcon} from "./components/ActiveIcon";
import {SoundButtonDisabledIcon} from "./components/DisabledIcon";

export const SoundButton = (props) => {

    const icon = props.soundEnabled ? <SoundButtonActiveIcon/> : <SoundButtonDisabledIcon/>;
    return <Wrapper {...props}>
        {icon}
    </Wrapper>;
};
