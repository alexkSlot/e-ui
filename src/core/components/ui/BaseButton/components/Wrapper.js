import styled from "styled-components";

export const Wrapper = styled.button`
  overflow: hidden;
  outline: none;
  cursor: pointer;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  user-select: none;
  box-sizing: border-box;
`;
