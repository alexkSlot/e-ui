import * as React from "react";
import { handleEvent } from "../../../../utils/handleEvents/handleEvents";
import { Wrapper } from "./components/Wrapper";

export const BaseButton = ({ children, ...props }) => (
    <Wrapper
        {...props}
        data-api-name={props.apiName}
        onClick={handleEvent(props.onClick)}
        onMouseEnter={handleEvent(props.onMouseEnter)}
        onMouseLeave={handleEvent(props.onMouseLeave)}
        onMouseDown={handleEvent(props.onMouseDown)}
        onMouseUp={handleEvent(props.onMouseUp)}
        onTouchStart={handleEvent(props.onTouchStart)}
        onTouchEnd={handleEvent(props.onTouchEnd)}
        role="button"
    >
        {children}
    </Wrapper>
);
