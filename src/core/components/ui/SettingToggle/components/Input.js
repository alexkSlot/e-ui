import styled from "styled-components";

export const Input = styled.input`
    height: 0;
	width: 0;
	visibility: hidden;
  
  &:checked + label{
  	background-color: #bada55;
  }
  
  
  &:checked + label{
  	&::after {
    left: calc(100% - 3px);
	transform: translateX(-100%);
    }
  }
`;
