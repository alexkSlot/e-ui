import styled from "styled-components";

export const Label = styled.label`
    cursor: pointer;
	text-indent: -9999px;
	width: 50px;
	height: 30px;
	background-color: grey;
	display: block;
	border-radius: 100px;
	position: relative;
	
	&::after {
         content: '';
	     position: absolute;
	     top: 3px;
	     left: 3px;
	     width: 24px;
	     height: 24px;
	     background-color: #fff;
	     border-radius: 90px;
	     transition: all 0.3s ease;
    }
    
    &:active {
      &::after {
          width: 35px;
      }
  }
`;
