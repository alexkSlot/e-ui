import React from "react";

import {Wrapper} from "./components/Wrapper";
import {Input} from "./components/Input";
import {Label} from "./components/Label";

export const SettingToggle = (props) => {
        return (
            <Wrapper {...props}>
                <Input
                    id={props.id}
                    checked={props.checked}
                    onChange={props.onChange}
                    type={props.type}
                />
                <Label for={props.id} onClick={props.onChange}/>
            </Wrapper>
        );
};
