import styled from "styled-components";

export const Wrapper = styled.svg`
  width: 100%;
  height: 100%;
  overflow: hidden;
  fill: currentColor;
  vertical-align: top;
`;
