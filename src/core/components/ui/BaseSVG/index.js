import * as React from "react";
import { Wrapper } from "./components/Wrapper";

export const BaseSVG = ({ children, ...props }) => (
    <Wrapper {...props}>{children}</Wrapper>
);
