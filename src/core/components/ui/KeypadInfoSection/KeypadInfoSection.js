import React from "react";
import styled from "styled-components";

const Wrapper = styled.div`
    color: #fff;
    text-align: center;
    overflow: hidden;
    outline: none;
    user-select: none;
`;

const Label = styled.span`
    font-size: 1.5vw;
`;

const TextHolder = styled.span`
    font-size: 2.2vw;
    display: block;
`;

export const KeypadInfoSection = ({ label, textValue }) => {
    return (
        <Wrapper>
            <Label>{label}</Label>
            <TextHolder>{textValue}</TextHolder>
        </Wrapper>

    );

};
