import React, {Component} from 'react';
//import './App.css';

import Keypad from "./containers/Keypad/Keypad";
import MenuContainer from "./containers/Menu/index";

class App extends Component {
    render() {
        return (
            <div style={{ pointerEvents: "auto" }}>
                <Keypad/>
                <MenuContainer/>
            </div>
        );
    }
}

export default App;
