import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {injectGlobal} from 'styled-components';

import {createStore, applyMiddleware, compose} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";

import {rootReducer} from "./store/reducers/rootReducer";
import {applySettings} from "../utils/applySettings/applySettings";

injectGlobal`
  body {
  margin: 0;
  padding: 0;
  font-family: sans-serif;
}

* {
   -webkit-touch-callout: none;
   user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   -webkit-user-select: none;
   box-sizing: inherit;
   margin: 0;
   padding: 0;
   border: none;
  }
  
  button {
      padding: 1px 7px 2px;
  }
`;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export const init = (initConfig) => {

    const MOUNT_NODE = initConfig.parentElement;

    // TODO: Use custom Errors and better validation techniques
    if (!(MOUNT_NODE && "nodeType" in MOUNT_NODE)) {
        throw new Error("PlayUI: options.el is not a valid DOM-element");
    }

    const render = () => {
        const app = (
            <Provider store={store}>
                <App/>
            </Provider>
        );

        ReactDOM.render(app, MOUNT_NODE);

        applySettings(initConfig);
    };

    render();
};



