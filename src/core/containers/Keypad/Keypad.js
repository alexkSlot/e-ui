import React, {Component} from "react";
import {connect} from "react-redux";
import {
    spinButtonClickHandler,
    toggleSoundButton,
    toggleMenuButton
    } from "../../store/actions/index";
import KeypadView from "../../components/KeypadView";

class Keypad extends Component {

    render() {
        const viewConfig = {
            onSpinButtonClicked: () => {
                this.props.onSpinButtonClicked()
            },
            toggleSoundButton: this.props.toggleSoundButton,
            toggleMenuButton: this.props.toggleMenuButton,

            currency: this.props.currency,
            balance: this.props.balance,
            win: this.props.win,
            keypadDisabled: !this.props.keypadEnabled,
            spinButtonDisabled: !this.props.spinButtonEnabled,
            autoPlayButtonDisabled: !this.props.autoPlayButtonEnabled,
            betButtonDisabled: !this.props.betButtonEnabled,
            menuButtonDisabled: !this.props.menuButtonEnabled,
            soundButtonDisabled: !this.props.soundButtonEnabled,
            soundEnabled: this.props.soundEnabled
        };

        return <KeypadView {...viewConfig}/>;
    }
}

const mapStateToProps = state => {
    return {
        currency: state.general.currency,
        balance: state.general.balance,
        win: state.general.win,

        keypadEnabled: state.keypad.keypadEnabled,
        spinButtonEnabled: state.keypad.spinButtonEnabled,
        autoPlayButtonEnabled: state.keypad.autoPlayButtonEnabled,
        betButtonEnabled: state.keypad.betButtonEnabled,
        menuButtonEnabled: state.keypad.menuButtonEnabled,
        soundButtonEnabled: state.keypad.soundButtonEnabled,
        soundEnabled: state.keypad.soundEnabled,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSpinButtonClicked: () => dispatch(spinButtonClickHandler()),
        toggleSoundButton: ()=>dispatch(toggleSoundButton()),
        toggleMenuButton: ()=>dispatch(toggleMenuButton())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Keypad);