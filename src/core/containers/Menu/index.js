import React, {Component} from "react";
import {connect} from "react-redux";
import {Menu} from "../../components/Menu";

import {sectionButtonClickHandler, gameSettingClickHandler} from "../../store/actions/menu/menuActions";

class MenuContainer extends Component {

    render() {
        const viewConfig = {
            menuActive: this.props.menuActive,
            activeSection: this.props.activeSection,
            onSectionButtonClicked: this.props.onSectionButtonClicked,
            onGameSettingChanged: this.props.onGameSettingChanged,

            gameSettings: this.props.gameSettings
        };

        return <Menu {...viewConfig}/>;
    }
}

const mapStateToProps = state => {
    return {
        menuActive: state.menu.menuActive,
        activeSection: state.menu.activeSection,
        gameSettings: state.menu.gameSettings
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onSectionButtonClicked: (sectionId) => dispatch(sectionButtonClickHandler(sectionId)),
        onGameSettingChanged: (settingId) => dispatch(gameSettingClickHandler(settingId))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuContainer);