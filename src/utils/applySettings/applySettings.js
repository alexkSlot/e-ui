import {API} from "../../api/api";
import {OuterApiEvents} from "../../api/outerApiEvents";

export const applySettings = (settings) => {
    //GENERAL

    if (settings.balance) {
        API.emit(OuterApiEvents.general.setBalance, settings.balance);
    }

    if (settings.win) {
        API.emit(OuterApiEvents.general.setWin, settings.win);
    }

    if (settings.currency) {
        API.emit(OuterApiEvents.general.setCurrency, settings.currency);
    }

    // KEYPAD

    if (settings.keypadDisabled) {
        API.emit(OuterApiEvents.keypad.disable);
    }

    if (settings.spinButtonDisabled) {
        API.emit(OuterApiEvents.spinButton.disable);
    }

    if (settings.menuButtonDisabled) {
        API.emit(OuterApiEvents.menuButton.disable);
    }

    if (settings.autoPlayButtonDisabled) {
        API.emit(OuterApiEvents.autoPlayButton.disable);
    }

    if (settings.betButtonDisabled) {
        API.emit(OuterApiEvents.betButton.disable);
    }

    if (settings.soundButtonDisabled) {
        API.emit(OuterApiEvents.soundButton.disable);
    }

    // GAME SETTINGS

    if(settings.gameSettings && Array.isArray(settings.gameSettings)){
        API.emit(OuterApiEvents.updateGameSettings, settings.gameSettings);
    }
};