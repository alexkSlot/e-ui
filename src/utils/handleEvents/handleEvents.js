import {API} from "../../api/api";

export const handleEvent = func => {
    return e => {
        const APIName = e.currentTarget.getAttribute("data-api-name");
        const disabled = e.currentTarget.hasAttribute("disabled");

        if (disabled) {
            return;
        }

        // Notify framework of the event
        if (typeof APIName === "string" && APIName !== "") {
            const event = (APIName + "." + e.type);

            //console.log(event);
            API.emit(event, e);
        }
        else {
            console.warn(
                'Missing API name. Make sure the component utilizes the "data-api-name" attribute.',
                "Element:" + e.currentTarget
            );
        }

        // Call original onclick handler if exists
        if (typeof func === "function") {
            func(e);
        }
    };
};
