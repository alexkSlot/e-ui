
export const addCurrencyPrefix = (textValue, currency) => {
    return `${textValue} (${currency})`
};