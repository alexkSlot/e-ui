
const THOUSAND_DIGITS = 3;

export const currencyFormatter = (cents, configuration) => {
    const {
        decimals,
        decimalDivider,
        thousandsDivider,
        symbol
    } = configuration;

    const [integer, fraction] = Math.abs(cents / 100)
        .toFixed(decimals)
        .split(".");

    const integerFormatted = integer
        .split("")
        .reverse()
        .map(
            (d, i) => (i > 0 && i % THOUSAND_DIGITS === 0 ? d + thousandsDivider : d)
        )
        .reverse()
        .join("");

    const amountFormatted = `${integerFormatted}${decimalDivider}${fraction}`;

    return `${symbol || ""}${cents < 0 ? "-" : ""}${amountFormatted}`;
};
