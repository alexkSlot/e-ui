import {APINames} from "./apiNames";

export const OuterApiEvents = {
    general: {
        setCurrency: "setCurrency",
        setBalance: "setBalance",
        setWin: "setWin"
    },
    keypad: {
        enable: `${APINames.KEYPAD}.enable`,
        disable: `${APINames.KEYPAD}.disable`
    },
    spinButton: {
        enable: `${APINames.SPIN_BUTTON}.enable`,
        disable: `${APINames.SPIN_BUTTON}.disable`
    },
    autoPlayButton: {
        enable: `${APINames.AUTO_PLAY_BUTTON}.enable`,
        disable: `${APINames.AUTO_PLAY_BUTTON}.disable`
    },
    betButton: {
        enable: `${APINames.BET_BUTTON}.enable`,
        disable: `${APINames.BET_BUTTON}.disable`
    },
    menuButton: {
        enable: `${APINames.MENU_BUTTON}.enable`,
        disable: `${APINames.MENU_BUTTON}.disable`
    },
    soundButton: {
        enable: `${APINames.SOUND_BUTTON}.enable`,
        disable: `${APINames.SOUND_BUTTON}.disable`
    },
    gameSettingChanged: "gameSettingChanged",
    updateGameSettings: "updateGameSettings"
};