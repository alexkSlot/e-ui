import emitter from "../services/emitter/emitter";

export const API = {

    on: (event, callback) => {
        emitter.on(event, callback);
    },

    off: (event, callback) => {
        emitter.off(event, callback);
    },

    emit: (event, value) => {
            emitter.emit(event, value);
       // emitter.emit(arguments);
    },

    /* Print a list of all actions exposed by PlayUI */
    listActions: () => {
        console.info(emitter.listActions());
    }
};