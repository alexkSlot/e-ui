export const APINames = {
    KEYPAD: "keypad",
    SPIN_BUTTON: "spinButton",
    AUTO_PLAY_BUTTON: "autoPlayButton",
    BET_BUTTON: "betButton",
    SOUND_BUTTON: "soundButton",
    MENU_BUTTON: "menuButton",
};
