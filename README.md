
# EUI - Esperanza User Interface

## Initialization

```js
EUI.init({
        parentElement: document.getElementById("root"),
        keypadDisabled: false,
        spinButtonDisabled: false,
        autoPlayButtonDisabled: false,
        betButtonDisabled: false,
        menuButtonDisabled: false,
        soundButtonDisabled: false,
        
        // USD by default if another isn't provided
        currency: { 
                    iso: 'EUR',
                    symbol: '€',
                    decimals: 2,
                    decimalDivider: '.',
                    thousandsDivider: ','
        },
        balance: 40000, // 0 by default
        win: 0, // 0 by default
        gameSettings: [ // featureSplashEnabled and quickSpinEnabled by default
            {
                id: "featureSplashEnabled",
                checked: false,
                type: "checkbox",
                label: "Show feature splash",
                icon: "featureSplash" // if the icon isn't provided the will be default one
            },
            {
                id: "quickSpinEnabled",
                checked: true,
                type: "checkbox",
                label: "Quick spin enabled",
                icon: "quickSpin"
            },
            {
                id: "introMovieEnabled",
                checked: false,
                type: "checkbox",
                label: "Intro Movie enabled"
            },
            {
                id: "sayFuckYouToSomeone",
                checked: true,
                type: "checkbox",
                label: "Say Fuck You to Some one"
            }
         ]
    });
```

## Outgoing events

##### Spin Button:
  - spinButton.mouseenter
  - spinButton.mouseleave
  - spinButton.mouseenter
  - spinButton.mousedown
  - spinButton.mouseup
  - spinButton.click
 
##### Auto Play Button:
 
- autoPlayButton.mouseenter
- autoPlayButton.mousedown
- autoPlayButton.mouseup
- autoPlayButton.click
- autoPlayButton.mouseleave
 
##### Bet Button:
  
 - betButton.mouseenter
 - betButton.mousedown
 - betButton.mouseup
 - betButton.click
 - betButton.mouseleave
 
##### Menu Button:
   
  - menuButton.mouseenter
  - menuButton.mousedown
  - menuButton.mouseup
  - menuButton.click
  - menuButton.mouseleave
  
##### Sound Button:
     
- soundButton.mouseenter
- soundButton.mousedown
- soundButton.mouseup
- soundButton.click
- soundButton.mouseleave
  
  
##### Game settings

 - gameSettingChanged - with argument:
```js
{
    settingId: "some string",
    value: boolean
}
```
  
  
 
## Incoming events

```js
var event = EUI.EVENTS.spinButton.enable;

var payload = "Kotik";

EUI.API.emit(event, payload);
```
### List of incoming events:

- setBalance
- setWin
- keypad.enable
- keypad.disable
- spinButton.enable
- spinButton.disable
- autoPlayButton.enable
- autoPlayButton.disable
- betButton.enable
- betButton.disable
- menuButton.enable
- menuButton.disable
- soundButton.enable
- soundButton.disable